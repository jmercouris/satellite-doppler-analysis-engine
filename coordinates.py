from __future__ import division
from PIL import Image

im = Image.open("output.png")
(width, height) = im.size

originX = 13.39
terminusX = 13.41
rangeX = terminusX - originX

originY = 52.51
terminusY = 52.53
rangeY = terminusY - originY

# Matrix
matrix = [[0 for x in range(width)] for y in range(height)] 

# Matrix Index
i = 0
j = 0
for pixel in iter(im.getdata()):
    matrix[i][j] = pixel
    i = i + 1
    if i >= width:
        i = 0
        j = j+1

for z in range (0, width):
    for n in range (0, height):
        li = matrix[z][n]
        sum = 0
        for q in li:
            sum = sum + q
        # Generate Coordinate String
        offsetX = rangeX * (z/width)
        offsetY = rangeY * (n/height)
        if ((int)((sum / 765) * 100) > 66):
            print "{location: new google.maps.LatLng(%f, %f), weight: %d}," % (offsetY + originY, offsetX + originX, ((sum / 765) * 100),)

